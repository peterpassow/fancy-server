# Fancy Server

## Introduction

This is a test project. It aims to explore dockerized applications and current cloud based CI/CD. 

## How to run

### Network
Create a custom network for fancy-server and fancy-db. Resolve IPs by container names. 

Docker: 
- docker network create --driver bridge fancy-net

### Database
Create a MySQL database container for the application user fsv. 

Docker: 
- docker run -d --name fancy-db --network fancy-net -p 3306:3306 -p 33060:33060 -e MYSQL_ROOT_PASSWORD=system -e MYSQL_DATABASE=fsv -e MYSQL_USER=fsv -e MYSQL_PASSWORD=mysql mysql:5.7.27 mysqld --character-set-server=utf8 --collation-server=utf8_unicode_ci

### Service
Run a Fancy Server container that connects to the fancy-db. Mount the volume fsv-config to the container path /opt/fsv/config for external configuration. 

Docker:

- docker volume create fsv-config
- docker run -d --name fancy-server --network fancy-net -v fsv-config:/opt/fsv/config -p 0.0.0.0:8080:8080/tcp peter78/fancy-server:0.0.7