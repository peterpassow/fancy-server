FROM openjdk:11.0.9.1-jre-buster

VOLUME ["/opt/fsv/config"]

COPY ["runFsv.sh", "fsv.jar", "/opt/fsv/bin/"] 

RUN chmod 777 /opt/fsv/bin/* && ls -lisa /opt/fsv/bin

WORKDIR /opt/fsv/

ENTRYPOINT ["bin/runFsv.sh"]
