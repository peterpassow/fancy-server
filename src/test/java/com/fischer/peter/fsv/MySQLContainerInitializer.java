package com.fischer.peter.fsv;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.MySQLContainer;

public class MySQLContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	@Override
	@SuppressWarnings("resource")
	public void initialize(ConfigurableApplicationContext applicationContext) {
		try {
			MySQLContainer<?> mysql = new MySQLContainer<>("mysql:5.7.27") //
					.withDatabaseName("fsv") //
					.withUsername("fsv") //
					.withPassword("mysql"); //

			mysql.start();

			String jdbcUrl = mysql.getJdbcUrl();
			String username = mysql.getUsername();
			String password = mysql.getPassword();

			TestPropertyValues.of( //
					"spring.datasource.url=" + jdbcUrl, //
					"spring.datasource.username=" + username, //
					"spring.datasource.password=" + password) //
					.applyTo(applicationContext.getEnvironment());

		} catch (Exception e) {
			// do nothing
		}
	}

}
