package com.fischer.peter.fsv;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(initializers = MySQLContainerInitializer.class)
class TestFsvApplication {

	@Test
	void contextLoads() {
	}

}
