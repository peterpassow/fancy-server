package com.fischer.peter.fsv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FsvApplication {

	public static void main(String[] args) {
		SpringApplication.run(FsvApplication.class, args);
	}

}
