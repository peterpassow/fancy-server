package com.fischer.peter.fsv;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DbConfig {

	private String jdbcUrl;
	private String username;
	private String password;

	public DbConfig(@Value("${spring.datasource.url}") String jdbcUrl,
			@Value("${spring.datasource.username}") String username,
			@Value("${spring.datasource.password}") String password) {
		super();
		this.jdbcUrl = jdbcUrl;
		this.username = username;
		this.password = password;
	}

	@Bean
	HikariDataSource hikariDataSource() {
		HikariConfig hikariConfig = new HikariConfig();
		hikariConfig.setJdbcUrl(jdbcUrl);
		hikariConfig.setUsername(username);
		hikariConfig.setPassword(password);

		return new HikariDataSource(hikariConfig);
	}

}
